defmodule ScoreSlam.GoalsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `ScoreSlam.Goals` context.
  """

  @doc """
  Generate a goal.
  """
  def goal_fixture(attrs \\ %{}) do
    {:ok, goal} =
      attrs
      |> Enum.into(%{
        goal_time: ~N[2024-06-02 09:45:00]
      })
      |> ScoreSlam.Goals.create_goal()

    goal
  end
end
