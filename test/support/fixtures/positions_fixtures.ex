defmodule ScoreSlam.PositionsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `ScoreSlam.Positions` context.
  """

  @doc """
  Generate a position.
  """
  def position_fixture(attrs \\ %{}) do
    {:ok, position} =
      attrs
      |> Enum.into(%{
        position_name: "some position_name"
      })
      |> ScoreSlam.Positions.create_position()

    position
  end
end
