defmodule ScoreSlam.GamePlayersFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `ScoreSlam.GamePlayers` context.
  """

  @doc """
  Generate a game_player.
  """
  def game_player_fixture(attrs \\ %{}) do
    {:ok, game_player} =
      attrs
      |> Enum.into(%{})
      |> ScoreSlam.GamePlayers.create_game_player()

    game_player
  end
end
