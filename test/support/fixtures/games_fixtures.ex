defmodule ScoreSlam.GamesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `ScoreSlam.Games` context.
  """

  @doc """
  Generate a game.
  """
  def game_fixture(attrs \\ %{}) do
    {:ok, game} =
      attrs
      |> Enum.into(%{
        end_time: ~N[2024-06-02 09:44:00],
        start_time: ~N[2024-06-02 09:44:00]
      })
      |> ScoreSlam.Games.create_game()

    game
  end
end
