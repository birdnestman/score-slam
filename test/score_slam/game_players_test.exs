defmodule ScoreSlam.GamePlayersTest do
  use ScoreSlam.DataCase

  alias ScoreSlam.GamePlayers

  describe "game_players" do
    alias ScoreSlam.GamePlayers.GamePlayer

    import ScoreSlam.GamePlayersFixtures

    @invalid_attrs %{}

    test "list_game_players/0 returns all game_players" do
      game_player = game_player_fixture()
      assert GamePlayers.list_game_players() == [game_player]
    end

    test "get_game_player!/1 returns the game_player with given id" do
      game_player = game_player_fixture()
      assert GamePlayers.get_game_player!(game_player.id) == game_player
    end

    test "create_game_player/1 with valid data creates a game_player" do
      valid_attrs = %{}

      assert {:ok, %GamePlayer{} = game_player} = GamePlayers.create_game_player(valid_attrs)
    end

    test "create_game_player/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = GamePlayers.create_game_player(@invalid_attrs)
    end

    test "update_game_player/2 with valid data updates the game_player" do
      game_player = game_player_fixture()
      update_attrs = %{}

      assert {:ok, %GamePlayer{} = game_player} =
               GamePlayers.update_game_player(game_player, update_attrs)
    end

    test "update_game_player/2 with invalid data returns error changeset" do
      game_player = game_player_fixture()

      assert {:error, %Ecto.Changeset{}} =
               GamePlayers.update_game_player(game_player, @invalid_attrs)

      assert game_player == GamePlayers.get_game_player!(game_player.id)
    end

    test "delete_game_player/1 deletes the game_player" do
      game_player = game_player_fixture()
      assert {:ok, %GamePlayer{}} = GamePlayers.delete_game_player(game_player)
      assert_raise Ecto.NoResultsError, fn -> GamePlayers.get_game_player!(game_player.id) end
    end

    test "change_game_player/1 returns a game_player changeset" do
      game_player = game_player_fixture()
      assert %Ecto.Changeset{} = GamePlayers.change_game_player(game_player)
    end
  end
end
