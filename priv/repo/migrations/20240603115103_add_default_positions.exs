defmodule ScoreSlam.Repo.Migrations.AddDefaultPositions do
  use Ecto.Migration

  def change do
    execute "INSERT INTO positions (position_name, inserted_at, updated_at) VALUES ('rot hinten', now(), now())"

    execute "INSERT INTO positions (position_name, inserted_at, updated_at) VALUES ('rot vorne', now(), now())"

    execute "INSERT INTO positions (position_name, inserted_at, updated_at) VALUES ('rot überall', now(), now())"

    execute "INSERT INTO positions (position_name, inserted_at, updated_at) VALUES ('blau hinten', now(), now())"

    execute "INSERT INTO positions (position_name, inserted_at, updated_at) VALUES ('blau vorne', now(), now())"

    execute "INSERT INTO positions (position_name, inserted_at, updated_at) VALUES ('blau überall', now(), now())"
  end
end
