defmodule ScoreSlam.Repo.Migrations.CreateGames do
  use Ecto.Migration

  def change do
    create table(:games) do
      add :start_time, :utc_datetime
      add :end_time, :utc_datetime
      add :state, :integer, default: 1

      timestamps(type: :utc_datetime)
    end
  end
end
