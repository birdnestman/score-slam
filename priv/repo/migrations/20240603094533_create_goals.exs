defmodule ScoreSlam.Repo.Migrations.CreateGoals do
  use Ecto.Migration

  def change do
    create table(:goals) do
      add :goal_time, :utc_datetime
      add :game_id, references(:games, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing)
      add :position_id, references(:positions, on_delete: :nothing)

      timestamps(type: :utc_datetime)
    end

    create index(:goals, [:game_id])
    create index(:goals, [:user_id])
    create index(:goals, [:position_id])
  end
end
