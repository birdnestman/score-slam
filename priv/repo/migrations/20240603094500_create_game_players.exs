defmodule ScoreSlam.Repo.Migrations.CreateGamePlayers do
  use Ecto.Migration

  def change do
    create table(:game_players) do
      add :game_id, references(:games, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing)
      add :position_id, references(:positions, on_delete: :nothing)

      timestamps(type: :utc_datetime)
    end

    create unique_index(:game_players, [:game_id, :user_id, :position_id])
  end
end
