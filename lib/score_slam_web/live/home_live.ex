defmodule ScoreSlamWeb.HomeLive do
  use ScoreSlamWeb, :live_view

  alias ScoreSlam.GameManager
  alias ScoreSlam.GameServer

  on_mount {ScoreSlamWeb.UserAuth, :mount_current_user}

  @impl true
  def mount(_params, _session, socket) do
    if connected?(socket), do: ScoreSlamWeb.Endpoint.subscribe("game")

    games = load_games()
    {:ok, assign(socket, games: games)}
  end

  defp load_games do
    GameManager.get_running_games()
    |> Enum.map(fn game_id ->
      case GameServer.get_state(game_id) do
        {:ok, game} -> game
        _ -> nil
      end
    end)
    |> Enum.filter(& &1)
  end

  @impl true
  def handle_info({:update_running_games, _running_games}, socket) do
    {:noreply, assign(socket, games: load_games())}
  end

  @impl true
  def handle_event("new_game", _params, socket) do
    GameManager.start_new_game(GameManager.generate_unique_id())
    {:noreply, socket}
  end
end
