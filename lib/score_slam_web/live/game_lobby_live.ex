defmodule ScoreSlamWeb.GameLobbyLive do
  use ScoreSlamWeb, :live_view

  alias ScoreSlam.Games
  alias ScoreSlam.GameServer

  require Logger

  on_mount {ScoreSlamWeb.UserAuth, :mount_current_user}

  @impl true
  def mount(%{"id" => reference}, _session, socket) do
    if connected?(socket), do: ScoreSlamWeb.Endpoint.subscribe("game:#{reference}")

    case GameServer.get_state(reference) do
      {:ok, game} ->
        socket =
          socket
          |> assign(:game, game)
          |> assign(:scores, calculate_scores(game.players))

        {:ok, socket}

      _ ->
        Logger.error("No game found with reference #{reference}")
        {:ok, socket |> push_redirect(to: "/")}
    end
  end

  @impl true
  def handle_info({:game_update, new_state}, socket) do
    socket = assign(socket, :game, new_state)

    if new_state.state == :running do
      {:noreply, push_redirect(socket, to: "/game/#{new_state.reference}/live")}
    else
      {:noreply, socket}
    end
  end

  @impl true
  def handle_event("new_game", _params, socket) do
    case Games.new_game() do
      {:ok, game} ->
        {:noreply, assign(socket, :game, game)}

      {:error, reason} ->
        Logger.error("Failed to create new game: #{inspect(reason)}")
        {:noreply, socket}
    end
  end

  @impl true
  def handle_event("start_game", _params, socket) do
    case socket.assigns.game do
      %{state: :open, reference: ref} ->
        ScoreSlam.GameServer.start_game(ref)
        {:noreply, socket}

      _ ->
        {:noreply, socket}
    end
  end

  defp calculate_scores(nil), do: %{red: 0, blue: 0}
  defp calculate_scores(%{}), do: %{red: 0, blue: 0}

  defp calculate_scores(goals) do
    Enum.reduce(goals, %{red: 0, blue: 0}, fn goal, acc ->
      if String.starts_with?(goal.position.position_name, "rot") do
        %{acc | red: acc.red + 1}
      else
        %{acc | blue: acc.blue + 1}
      end
    end)
  end
end
