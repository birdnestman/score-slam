defmodule ScoreSlamWeb.JoinLive do
  use ScoreSlamWeb, :live_view

  alias ScoreSlam.Positions
  alias ScoreSlam.GameServer

  require Logger

  on_mount {ScoreSlamWeb.UserAuth, :mount_current_user}

  @impl true
  def mount(%{"id" => reference}, _session, socket) do
    if connected?(socket), do: ScoreSlamWeb.Endpoint.subscribe("game:#{reference}")

    case GameServer.get_state(reference) do
      {:ok, %{state: :running, reference: reference}} ->
        {:ok, socket |> push_redirect(to: ~p"/game/#{reference}/live")}

      {:ok, %{state: :finished, reference: reference}} ->
        {:ok, socket |> push_redirect(to: ~p"/game/#{reference}")}

      {:ok, game} ->
        socket =
          socket
          |> assign(:game, game)
          |> assign(:positions, Positions.list_positions())
          |> assign(:current_user, socket.assigns.current_user)

        {:ok, socket}

      _ ->
        Logger.error("No game found with reference #{reference}")
        {:ok, socket |> push_redirect(to: "/")}
    end
  end

  @impl true
  def handle_event("select_position", %{"team" => team_str, "position" => position_str}, socket) do
    user = socket.assigns.current_user
    game = socket.assigns.game
    team = String.to_atom(team_str)
    position = String.to_atom(position_str)

    GameServer.update_player(game.reference, user.id, user.player_name, team, position)

    {:noreply, socket}
  end

  @impl true
  def handle_info({:game_update, new_state}, socket) do
    socket =
      socket
      |> assign(:game, new_state)
      |> push_redirect(to: ~p"/game/#{socket.assigns.game.reference}")

    {:noreply, socket}
  end

  defp get_position_id(positions, name) do
    position =
      Enum.find(positions, fn elem ->
        elem.position_name == name
      end)

    case position do
      nil -> raise "Position not found: #{name}"
      _ -> position.id
    end
  end
end
