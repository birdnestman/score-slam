defmodule ScoreSlamWeb.HistoryLive do
  use ScoreSlamWeb, :live_view

  alias ScoreSlam.Games
  alias ScoreSlam.GamePlayers
  alias ScoreSlam.Goals

  @impl true
  def mount(_params, _session, socket) do
    games = Games.list_finished_games()

    games_with_details =
      Enum.map(games, fn game ->
        game_players = GamePlayers.list_game_players(game.id)
        goals = Goals.list_goals(game.id)
        scores = calculate_scores(goals)
        %{game: game, players: game_players, goals: goals, scores: scores}
      end)

    socket =
      socket
      |> assign(:games_with_details, games_with_details)

    {:ok, socket}
  end

  defp calculate_scores(goals) do
    Enum.reduce(goals, %{red: 0, blue: 0}, fn goal, acc ->
      if String.starts_with?(goal.position.position_name, "rot") do
        %{acc | red: acc.red + 1}
      else
        %{acc | blue: acc.blue + 1}
      end
    end)
  end

  defp player_goals_count(goals, player_id) do
    Enum.count(goals, fn goal -> goal.user_id == player_id end)
  end
end
