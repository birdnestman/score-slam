defmodule ScoreSlamWeb.GameLive do
  use ScoreSlamWeb, :live_view

  alias ScoreSlam.GameServer

  require Logger

  @tick_interval 1_000

  @impl true
  def mount(%{"id" => reference}, _session, socket) do
    if connected?(socket) do
      ScoreSlamWeb.Endpoint.subscribe("game:#{reference}")
      schedule_tick()
    end

    player_lines = [:goalie_rod, :defense_rod, :midlefield_rod, :forward_rod]

    case GameServer.get_state(reference) do
      {:ok, game} ->
        socket =
          socket
          |> assign(:game, game)
          |> assign(:game_duration, "00:00")
          |> assign(:goals, [])
          |> assign(:player_lines, player_lines)
          |> assign(:reference, reference)

        {:ok, socket}

      _ ->
        Logger.error("No game found with reference #{reference}")
        {:ok, socket |> push_redirect(to: "/")}
    end

    # game = Games.get_current_game()
    # game_players = game && GamePlayers.list_game_players(game.id)
    # goals = game && Goals.list_goals(game.id)

    # socket =
    #   socket
    #   |> assign(:game, socket.assign.game)
    #   |> assign(:game_players, game_players)
    #   |> assign(:goals, goals)
    #   |> assign(:scores, calculate_scores(goals))
    #   |> assign(:start_time, DateTime.utc_now())

    # {:ok, socket}
  end

  @impl true
  def handle_info(:tick, socket) do
    if socket.assigns.game do
      if socket.assigns.game.state == :running do
        game_duration = calculate_game_duration(socket.assigns.game.start_time)
        socket = assign(socket, :game_duration, game_duration)

        schedule_tick()
        {:noreply, socket}
      else
        {:noreply, socket}
      end
    else
      {:noreply, socket}
    end
  end

  @impl true
  def handle_info({:game_update, new_state}, socket) do
    socket = assign(socket, :game, new_state)

    if new_state.state == :finished do
      {:noreply, push_redirect(socket, to: "/game/#{new_state.reference}")}
    else
      {:noreply, socket}
    end
  end

  @impl true
  def handle_event(
        "track_goal",
        %{"player_line" => player_line, "team" => team} = _params,
        socket
      ) do
    {:ok, new_state} = GameServer.track_goal(socket.assigns.reference, team, player_line)
    socket = assign(socket, :game, new_state)
    {:noreply, socket}
  end

  @impl true
  def handle_event("delete_goal", %{"goal_index" => goal_index}, socket) do
    index =
      case goal_index do
        i when is_binary(i) -> String.to_integer(i)
        i -> i
      end

    {:ok, new_state} = GameServer.drop_goal(socket.assigns.reference, index)

    socket = assign(socket, :game, new_state)
    {:noreply, socket}
  end

  @impl true
  def handle_event("finish_game", _value, socket) do
    GameServer.finish_game(socket.assigns.reference)
    {:noreply, socket}
  end

  defp schedule_tick, do: Process.send_after(self(), :tick, @tick_interval)

  defp calculate_game_duration(start_time) do
    now = DateTime.utc_now()
    duration = DateTime.diff(now, start_time, :second)

    minutes = div(duration, 60)
    seconds = rem(duration, 60)

    formatted_time =
      "#{pad_zero(minutes)}:#{pad_zero(seconds)}"

    formatted_time
  end

  defp pad_zero(value) when value < 10, do: "0#{value}"
  defp pad_zero(value), do: "#{value}"

  defp format_goal_time(start_time, goal_time) do
    diff = DateTime.diff(goal_time, start_time, :second)
    minutes = div(diff, 60)
    seconds = rem(diff, 60)

    "#{pad_zero(minutes)}:#{pad_zero(seconds)}"
  end
end
