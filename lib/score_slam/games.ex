defmodule ScoreSlam.Games do
  @moduledoc """
  The Games context.
  """

  import Ecto.Query, warn: false
  alias ScoreSlam.Repo

  alias ScoreSlam.Games.Game

  @doc """
  Returns the list of games.

  ## Examples

      iex> list_games()
      [%Game{}, ...]

  """
  def list_games do
    Repo.all(Game)
  end

  @doc """
  Gets a single game.

  Raises `Ecto.NoResultsError` if the Game does not exist.

  ## Examples

      iex> get_game!(123)
      %Game{}

      iex> get_game!(456)
      ** (Ecto.NoResultsError)

  """
  def get_game!(id), do: Repo.get!(Game, id)

  def get_current_game(state) do
    Repo.one(from g in Game, where: g.state == ^state, limit: 1)
  end

  def list_finished_games do
    Repo.all(from g in Game, where: g.state == :closed, order_by: [desc: g.end_time])
  end

  @doc """
  Creates a game.

  ## Examples

      iex> create_game(%{field: value})
      {:ok, %Game{}}

      iex> create_game(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_game(attrs \\ %{}) do
    %Game{}
    |> Game.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a game.

  ## Examples

      iex> update_game(game, %{field: new_value})
      {:ok, %Game{}}

      iex> update_game(game, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_game(%Game{} = game, attrs) do
    game
    |> Game.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a game.

  ## Examples

      iex> delete_game(game)
      {:ok, %Game{}}

      iex> delete_game(game)
      {:error, %Ecto.Changeset{}}

  """
  def delete_game(%Game{} = game) do
    Repo.delete(game)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking game changes.

  ## Examples

      iex> change_game(game)
      %Ecto.Changeset{data: %Game{}}

  """
  def change_game(%Game{} = game, attrs \\ %{}) do
    Game.changeset(game, attrs)
  end

  def get_current_game do
    Repo.one(from g in Game, where: is_nil(g.end_time), limit: 1)
  end

  def new_game do
    %Game{}
    |> Repo.insert()
  end

  def start_game(%Game{state: :open} = game) do
    start_time = DateTime.utc_now() |> DateTime.truncate(:second)

    game
    |> Game.changeset(%{state: :running, start_time: start_time})
    |> Repo.update()
  end

  def start_game(game), do: {:ok, game}

  def end_game(%Game{} = game) do
    end_time = DateTime.utc_now() |> DateTime.truncate(:second)

    game
    |> Game.changeset(%{end_time: end_time, state: :closed})
    |> Repo.update()
  end
end
