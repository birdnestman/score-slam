defmodule ScoreSlam.Goals do
  import Ecto.Query, warn: false
  alias ScoreSlam.Repo

  alias ScoreSlam.Goals.Goal

  def list_goals do
    Repo.all(Goal)
  end

  def get_goal!(id), do: Repo.get!(Goal, id)

  def list_goals(game_id) do
    Repo.all(
      from g in Goal,
        where: g.game_id == ^game_id,
        order_by: [desc: g.inserted_at],
        preload: [:user, :position]
    )
  end

  def create_goal(attrs \\ %{}) do
    %Goal{}
    |> Goal.changeset(attrs)
    |> Repo.insert()
  end

  def update_goal(%Goal{} = goal, attrs) do
    goal
    |> Goal.changeset(attrs)
    |> Repo.update()
  end

  def delete_goal(%Goal{} = goal) do
    Repo.delete(goal)
  end

  def change_goal(%Goal{} = goal) do
    Goal.changeset(goal, %{})
  end
end
