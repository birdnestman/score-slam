defmodule ScoreSlam.Games.Game do
  use Ecto.Schema
  import Ecto.Changeset

  schema "games" do
    field :start_time, :utc_datetime
    field :end_time, :utc_datetime
    field :state, Ecto.Enum, values: [open: 1, running: 2, closed: 3]
    timestamps()
  end

  @doc false
  def changeset(game, attrs) do
    game
    |> cast(attrs, [:start_time, :end_time, :state])
    |> validate_required([:start_time])
  end
end
