defmodule ScoreSlam.GamePlayers do
  @moduledoc """
  The GamePlayers context.
  """

  import Ecto.Query, warn: false
  alias ScoreSlam.Repo

  alias ScoreSlam.GamePlayers.GamePlayer

  def list_game_players(game_id) do
    Repo.all(
      from gp in GamePlayer,
        where: gp.game_id == ^game_id,
        preload: [:user, :position]
    )
  end

  def get_game_player!(id), do: Repo.get!(GamePlayer, id)

  def get_game_player_for_user_and_game(user_id, game_id) do
    Repo.get_by(GamePlayer, user_id: user_id, game_id: game_id)
  end

  def create_game_player(attrs \\ %{}) do
    %GamePlayer{}
    |> GamePlayer.changeset(attrs)
    |> Repo.insert()
  end

  def update_game_player(%GamePlayer{} = game_player, attrs) do
    game_player
    |> GamePlayer.changeset(attrs)
    |> Repo.update()
  end

  def delete_game_player(%GamePlayer{} = game_player) do
    Repo.delete(game_player)
  end

  def change_game_player(%GamePlayer{} = game_player) do
    GamePlayer.changeset(game_player, %{})
  end
end
