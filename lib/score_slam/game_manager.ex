defmodule ScoreSlam.GameManager do
  def start_new_game(game_id) do
    ScoreSlam.GameSupervisor.start_game(game_id)
    broadcast_game_update()
  end
  
  def finish_game(game_id) do
    ScoreSlam.GameServer.finish_game(game_id)
  end

  def get_running_games do
    Registry.select(ScoreSlam.GameRegistry, [{{:"$1", :_, :_}, [], [:"$1"]}])
  end

  def generate_unique_id(num_words \\ 3) do
    words = ~w(cat dog sun fox car toy run sky bed bug bat red cup)

    words
    |> Enum.shuffle()
    |> Enum.take(num_words)
    |> Enum.join("-")
  end

  defp broadcast_game_update do
    Phoenix.PubSub.broadcast(
      ScoreSlam.PubSub,
      "game",
      {:update_running_games, get_running_games()}
    )
  end
end
