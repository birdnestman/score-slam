defmodule ScoreSlam.GameServer do
  @module_tag :"m_#{__MODULE__}"
  @module_impl ScoreSlam.GameServer.Implementation

  @callback start_link(game_id :: String.t()) :: {:ok, pid()} | {:error, term()}

  def start_link(game_id), do: impl().start_link(game_id)
  def get_state(game_id), do: impl().get_state(game_id)
  def start_game(game_id), do: impl().start_game(game_id)
  def finish_game(game_id), do: impl().finish_game(game_id)
  def track_goal(game_id, team, player_line), do: impl().track_goal(game_id, team, player_line)
  def drop_goal(game_id, goal_index), do: impl().drop_goal(game_id, goal_index)

  def update_player(game_id, user_id, name, team, position),
    do: impl().update_player(game_id, user_id, name, team, position)

  def impl(), do: Application.get_env(:score_slam, @module_tag, @module_impl)
end

defmodule ScoreSlam.GameServer.State do
  @moduledoc false

  @type t :: %__MODULE__{
          game_id: String.t(),
          reference: String.t(),
          goals: [goal()],
          score: %{red: integer(), blue: integer()},
          players: players(),
          state: Atom.t(),
          start_time: DateTime.t(),
          end_time: DateTime.t()
        }

  @type goal :: %{
          player: player(),
          player_line: atom(),
          team: atom(),
          timestamp: DateTime.t()
        }

  @type player :: %{
          id: String.t(),
          name: String.t(),
          position: String.t()
        }

  @type state :: :open | :running | :closed

  @type players :: %{red: [player()], blue: [player()]}

  @type game_id :: String.t()

  defstruct game_id: nil,
            reference: "",
            goals: [],
            score: %{red: 0, blue: 0},
            players: %{red: [], blue: []},
            state: :open,
            start_time: DateTime.utc_now(),
            end_time: nil
end

defmodule ScoreSlam.GameServer.Implementation do
  use GenServer

  # Client API

  def start_link(game_id) do
    GenServer.start_link(__MODULE__, game_id, name: via_tuple(game_id))
  end

  defp via_tuple(game_id), do: {:via, Registry, {ScoreSlam.GameRegistry, game_id}}

  def track_goal(game_id, team, player_line) do
    GenServer.call(via_tuple(game_id), {:track_goal, team, player_line})
  end

  def drop_goal(game_id, goal_index) do
    GenServer.call(via_tuple(game_id), {:drop_goal, goal_index})
  end

  def start_game(game_id) do
    GenServer.call(via_tuple(game_id), :start_game)
  end

  def finish_game(game_id) do
    GenServer.cast(via_tuple(game_id), :finish_game)
  end

  def get_state(game_id) do
    GenServer.call(via_tuple(game_id), :get_state)
  end

  def update_player(game_id, user_id, name, team, position) do
    GenServer.call(via_tuple(game_id), {:update_player, user_id, name, team, position})
  end

  # Server Callbacks

  def init(game_id) do
    Registry.register(ScoreSlam.GameRegistry, game_id, nil)

    {:ok,
     %ScoreSlam.GameServer.State{
       reference: game_id,
       state: :open
     }}
  end

  def handle_call({:add_point, player, score}, _from, state) do
    timestamp = DateTime.utc_now()
    point = %{player: player, score: score, timestamp: timestamp}
    {:reply, :ok, %{state | points: [point | state.points]}}
  end

  def handle_call(:get_state, _from, state) do
    {:reply, {:ok, state}, state}
  end

  def handle_call(:start_game, _from, state) do
    if state.state == :open do
      new_state = %{state | state: :running}

      broadcast_game_update(new_state)
      {:reply, {:ok, new_state}, new_state}
    else
      {:reply, {:error, :already_running_or_closed}, state}
    end
  end

  def handle_call({:update_player, user_id, player_name, team, position}, _from, state) do
    players =
      state.players
      |> remove_user_from_team(user_id, :red)
      |> remove_user_from_team(user_id, :blue)

    new_player = %{user_id: user_id, player_name: player_name, position: position}
    updated_team = (players[team] || []) ++ [new_player]
    updated_players = Map.put(players, team, updated_team)
    new_state = %{state | players: updated_players}

    broadcast_game_update(new_state)

    {:reply, {:ok, new_state}, new_state}
  end

  def handle_call({:track_goal, team, player_line}, _from, state) do
    team_atom =
      case team do
        t when is_binary(t) -> String.to_existing_atom(t)
        t -> t
      end

    rod =
      case player_line do
        pl when is_binary(pl) -> String.to_existing_atom(pl)
        pl -> pl
      end

    team_players = Map.get(state.players, team_atom, [])
    eligible_players = eligible_players_for_rod(team_players, rod)

    selected_player =
      case eligible_players do
        [] -> nil
        [player | _rest] -> player
      end

    new_goal = %{
      player: selected_player,
      player_line: rod,
      team: team_atom,
      timestamp: DateTime.utc_now()
    }

    new_goals = [new_goal | state.goals]
    score = calculate_scores(new_goals)

    new_state = %{state | goals: new_goals, score: score}
    broadcast_game_update(new_state)
    {:reply, {:ok, new_state}, new_state}
  end

  def handle_call({:drop_goal, goal_index}, _from, state) do
    new_goals = List.delete_at(state.goals, goal_index)
    score = calculate_scores(new_goals)
    new_state = %{state | goals: new_goals, score: score}
    broadcast_game_update(new_state)
    {:reply, {:ok, new_state}, new_state}
  end

  def handle_cast(:finish_game, state) do
    if state.state == :running do
      new_state = %{state | state: :finished, end_time: DateTime.utc_now()}

      broadcast_game_update(new_state)
      {:noreply, new_state}
    else
      {:noreply, state}
    end
  end

  defp remove_user_from_team(players, user_id, team_atom) do
    updated_team = Enum.reject(players[team_atom] || [], &(&1.user_id == user_id))
    Map.put(players, team_atom, updated_team)
  end

  defp calculate_scores(goals) do
    Enum.reduce(goals, %{red: 0, blue: 0}, fn goal, acc ->
      if goal.team == :red do
        %{acc | red: acc.red + 1}
      else
        %{acc | blue: acc.blue + 1}
      end
    end)
  end

  defp broadcast_game_update(new_state) do
    Phoenix.PubSub.broadcast(
      ScoreSlam.PubSub,
      "game:#{new_state.reference}",
      {:game_update, new_state}
    )
  end

  defp eligible_players_for_rod(players, :goalie_rod) do
    Enum.filter(players, fn player ->
      player.position in [:all]
    end)
  end

  defp eligible_players_for_rod(players, :defense_rod) do
    Enum.filter(players, fn player ->
      player.position in [:all, :back]
    end)
  end

  defp eligible_players_for_rod(players, :midlefield_rod) do
    Enum.filter(players, fn player ->
      player.position in [:all, :front]
    end)
  end

  defp eligible_players_for_rod(players, :forward_rod) do
    Enum.filter(players, fn player ->
      player.position in [:all, :front]
    end)
  end
end
