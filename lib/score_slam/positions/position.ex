defmodule ScoreSlam.Positions.Position do
  use Ecto.Schema
  import Ecto.Changeset

  schema "positions" do
    field :position_name, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(position, attrs) do
    position
    |> cast(attrs, [:position_name])
    |> validate_required([:position_name])
  end
end
