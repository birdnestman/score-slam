defmodule ScoreSlam.GamePlayers.GamePlayer do
  use Ecto.Schema
  import Ecto.Changeset

  schema "game_players" do
    belongs_to :game, ScoreSlam.Games.Game
    belongs_to :user, ScoreSlam.Accounts.User
    belongs_to :position, ScoreSlam.Positions.Position

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(game_player, attrs) do
    game_player
    |> cast(attrs, [:game_id, :user_id, :position_id])
    |> validate_required([:game_id, :user_id, :position_id])
    |> unique_constraint([:game_id, :user_id, :position_id])
  end
end
