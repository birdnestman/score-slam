defmodule ScoreSlam.Goals.Goal do
  use Ecto.Schema
  import Ecto.Changeset

  schema "goals" do
    field :goal_time, :utc_datetime

    belongs_to :game, ScoreSlam.Games.Game
    belongs_to :user, ScoreSlam.Accounts.User
    belongs_to :position, ScoreSlam.Positions.Position

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(goal, attrs) do
    goal
    |> cast(attrs, [:game_id, :user_id, :position_id, :goal_time])
    |> validate_required([:game_id, :user_id, :position_id, :goal_time])
  end
end
