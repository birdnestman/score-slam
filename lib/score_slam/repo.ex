defmodule ScoreSlam.Repo do
  use Ecto.Repo,
    otp_app: :score_slam,
    adapter: Ecto.Adapters.Postgres
end
